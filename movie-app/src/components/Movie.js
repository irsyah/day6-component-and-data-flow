function Movie({ title }) {
    return(
      <div>
        <h1>Movie: {title}</h1>
      </div>
    )
}

export default Movie;